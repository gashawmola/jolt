import React, { Component } from 'react';
import './App.css';
import { Navbar, Grid, Row, Col } from 'react-bootstrap';
import NewForm from './NewForm';
import Timetable from './Timetable';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar inverse>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#">Scheduler</a>
            </Navbar.Brand>
          </Navbar.Header>
        </Navbar>
        <Grid>
          <Row>
            <Col lg={ 4 }>
              <NewForm />
            </Col>
            <Col lg={ 8 }>
              <Timetable />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
