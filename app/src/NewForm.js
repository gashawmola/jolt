import React, { Component } from 'react';
import { Panel, FormGroup, FormControl, ControlLabel, HelpBlock, Button } from 'react-bootstrap';

const timestampToString = (timestamp) => (new Date(timestamp)).toISOString().slice(0, -8);
const stringToTimestamp = (str) => (new Date(str)).getTime();

class NewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      data: {
        task: '',
        start: (new Date()).getTime(),
        interval: 0
      },
      submitting: false
    };
    this.handleTaskChange = this.handleTaskChange.bind(this);
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleIntervalChange = this.handleIntervalChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    fetch('/api/tasks')
      .then(res => res.json())
      .then(tasks => this.setState(({ data }) => ({ tasks, data: { ...data, task: tasks[0]} })));
  }

  handleTaskChange({ target }) {
    this.setState(({ data }) => ({ data: { ...data, task: target.value }}));
  }

  handleStartChange({ target }) {
    this.setState(({ data }) => ({ data: { ...data, start: stringToTimestamp(target.value) }}));
  }

  handleIntervalChange({ target }) {
    this.setState(({ data }) => ({ data: { ...data, interval: +target.value }}));
  }

  handleSubmit(e) {
    e.preventDefault();
    const { submitting, data } = this.state;
    if (submitting) return;
    this.setState({ submitting: true });
    fetch('/api/scheduled-tasks', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'content-type': 'application/json' }
    })
      .then((res) => {
        if (res.ok) return alert('task add to schedule!');
        return Promise.reject();
      })
      .catch(() => alert('failed to add task to schedule!'))
      .finally(() => this.setState({ submitting: false }));
  }

  render() {
    const { tasks, data: { task, start, interval } } = this.state;
    return (
      <Panel bsStyle="primary">
        <Panel.Heading>
          <Panel.Title componentClass="h3">Add task to schedule</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <form onSubmit={ this.handleSubmit }>
            <FormGroup controlId="task">
              <ControlLabel>Task</ControlLabel>
              <FormControl componentClass="select" value={ task } onChange={ this.handleTaskChange } required>
                { tasks.map( (task) => <option key={ task } value={ task }>{ task }</option> ) }
              </FormControl>
            </FormGroup>
            <FormGroup controlId="start">
              <ControlLabel>Start Time (GMT)</ControlLabel>
              <FormControl type="datetime-local" value={ timestampToString(start) } onChange={ this.handleStartChange } required/>
            </FormGroup>
            <FormGroup controlId="interval">
              <ControlLabel>Repeat Interval (ms)</ControlLabel>
              <FormControl type="integer" value={ interval } onChange={ this.handleIntervalChange } min="0"/>
              <HelpBlock>Enter '0' to not repeat task</HelpBlock>
            </FormGroup>
            <Button type="submit">Add</Button>
          </form>
        </Panel.Body>
      </Panel>
    );
  }

}

export default NewForm;
