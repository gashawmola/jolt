import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class Timetable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      scheduledTasks: []
    };
    this.interval = null;
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData().finally(() => this.interval = setInterval(this.fetchData, 500));
  }

  componentWillUnMount() {
    clearInterval(this.interval);
  }

  fetchData() {
    return fetch('/api/scheduled-tasks')
      .then(res => res.ok && res.json() || Promise.reject())
      .then(scheduledTasks => this.setState({ scheduledTasks }));
  }

  render() {
    const { scheduledTasks } = this.state;
    return (
      <Table striped bordered condensed hover>
        <caption>Timetable</caption>
        <thead>
          <tr>
            <th>Next</th>
            <th>Task</th>
            <th>Interval</th>
          </tr>
        </thead>
        <tbody>
          { scheduledTasks.map(([ nextTime, scheduledTask ]) =>
            <tr key={ scheduledTask.id }>
              <td>{ (new Date(nextTime)).toLocaleString() }</td>
              <td>{ scheduledTask.task }</td>
              <td>{ scheduledTask.interval }</td>
            </tr>
            )
          }
        </tbody>
      </Table>
    );
  }
}

export default Timetable;
