const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const redis = require('redis');
const Timetable = require('./lib/timetable');
const RunQueue = require('./lib/run-queue');
const Scheduler = require('./lib/scheduler');
const Worker = require('./lib/worker');

const tasks = {};
fs.readdirSync(path.join(__dirname, 'tasks')).forEach((file) => {
  if(file.endsWith('.js')) tasks[ file.slice(0, -3) ] = require('./tasks/' + file);
});

const REDIS_URL = process.env.REDIS_URL || 'redis://127.0.0.1:6379/';
const redisClient = redis.createClient(REDIS_URL);

const timetable = new Timetable(redisClient);
const runQueue = new RunQueue(redisClient);
const scheduler = new Scheduler(timetable, runQueue);
const worker = new Worker(runQueue, tasks);

scheduler.start();
worker.start();

const app = express();
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'app/build')));

app.get('/api/tasks', async (req, res) => {
  res.json(Object.keys(tasks));
});

app.get('/api/scheduled-tasks', async (req, res) => {
  res.json(await timetable.getAll());
});

app.post('/api/scheduled-tasks', async (req, res) => {
  let { task, params, start, interval } = req.body;
  start = parseInt(start, 10);
  interval = parseInt(interval, 10) || 0;
  if (!tasks[task] || interval < 0) return res.status(422).json({ error: 'Invalid params' });
  const id = await timetable.add( task, { params, start, interval });
  res.json({ id });
});

app.delete('/api/scheduled-tasks/:id', async (req, res) => {
  const scheduledTask = await timetable.get(req.params.id);
  if (!scheduledTask) return res.status(404).json({ error: 'Not found' });
  await timetable.remove(scheduledTask.id);
  res.json(scheduledTask);
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'app/build/index.html'));
});

const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => console.log('Listening on port ' + PORT));
