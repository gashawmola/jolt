
const RUN_QUEUE_KEY = 'RUN_QUEUE';

class RunQueue {

  constructor ( redisClient ) {
    this._redisClient = redisClient;
  }

  enqueue ( task, params ) {
    return new Promise( ( resolve, reject ) => {
      this._redisClient.rpush(
        RUN_QUEUE_KEY,
        JSON.stringify( [ task, params ] ),
        ( err ) => err ? reject( err ) : resolve()
      );
    } );
  }

  dequeue () {
    return new Promise( ( resolve, reject ) => {
      this._redisClient.lpop(
        RUN_QUEUE_KEY,
        ( err, result ) => err ? reject( err ) : resolve( JSON.parse( result ) )
      );
    } );
  }

}

module.exports = RunQueue;
