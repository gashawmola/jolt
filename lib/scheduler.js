
const SCHEDULE_INTERVAL = 1000;

class Scheduler {

  constructor ( timetable, runQueue ) {
    this._timetable = timetable;
    this._runQueue = runQueue;
    this._interval = null;
    this.schedule = this.schedule.bind( this );
  }

  async schedule () {
    const now = ( new Date() ).getTime();
    while ( true ) {
      let [ id, timestamp ] = await this._timetable.peak();
      if ( !id || timestamp > now ) {
        break;
      }
      let { task, params, interval } = await this._timetable.get( id );
      await this._runQueue.enqueue( task, params );
      if ( interval > 0 ) {
        await this._timetable.increment( id, interval );
      } else {
        await this._timetable.remove( id );
      }
    }
  }

  start () {
    this._interval = setInterval( this.schedule, SCHEDULE_INTERVAL );
  }

  stop () {
    if ( this._interval ) {
      clearInterval( this._interval );
      this._interval = null;
    }
  }

}

module.exports = Scheduler;
