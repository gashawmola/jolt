const uuid = require('uuid/v1');

const ORDERED_TASK_SCHEDULES_SET = 'ORDERED_TASK_SCHEDULES';

const KEY_PREFIX = 'task-schedule';
const prefix = function ( key ) {
  return KEY_PREFIX + ':' + key;
};

const unflattenPairs = function ( array ) {
  const result = [];
  for ( let i = 0; i < array.length; i += 2 ) {
    result.push( [ array[ i ], array[ i + 1 ] ] );
  }
  return result;
}

class Timetable {

  constructor ( redisClient ) {
    this._redisClient = redisClient;
  }

  add ( task, { params , start, interval } = {} ) {
    params = params || {};
    start = start || ( new Date() ).getTime();
    interval = interval || 0;
    const id = uuid();
    const schedule = { id, params, task, start, interval };
    return new Promise( ( resolve, reject ) => {
      this._redisClient.multi()
        .set( prefix( id ), JSON.stringify( schedule ) )
        .zadd( ORDERED_TASK_SCHEDULES_SET, start, id )
        .exec( ( err ) => err ? reject( err ) : resolve( id ) );
    } );
  }

  get ( id ) {
    return new Promise( ( resolve, reject ) => {
      this._redisClient
        .get(
          prefix( id ),
          ( err, result ) => {
            if ( err ) return reject( err );
            const schedule = JSON.parse( result );
            resolve( schedule );
          } );
    } );
  }

  remove ( id ) {
    return new Promise( ( resolve, reject ) => {
      this._redisClient.multi()
        .zrem( ORDERED_TASK_SCHEDULES_SET, id )
        .del( prefix( id ) )
        .exec( ( err ) => err ? reject( err ) : resolve() );
    } );
  }

  // TODO: typo
  peak () {
    return new Promise( ( resolve, reject ) => {
      this._redisClient
        .zrange(
          ORDERED_TASK_SCHEDULES_SET,
          0,
          0,
          'WITHSCORES',
          ( err, [ id, start ] ) => {
            if ( err ) return reject( err );
            if ( id ) {
              resolve( [ id, +start ] );
            } else {
             resolve( [] );
            }
          } );
    } );
  }

  increment ( id, delta ) {
    return new Promise( ( resolve, reject ) => {
      this._redisClient.watch( id, ( err ) => {
        if ( err ) return reject( err );
        this._redisClient.multi()
          .zincrby( ORDERED_TASK_SCHEDULES_SET, delta, id )
          .exec( ( err, results ) => err ? reject( err ) : resolve( +results[ 0 ] ) );
      } );
    } );
  }

  // TODO: test it
  getAll () {
    return new Promise( (resolve, reject) => {
      this._redisClient
        .zrange(
          ORDERED_TASK_SCHEDULES_SET,
          0,
          -1,
          'WITHSCORES',
          ( err, results ) => {
            if ( err ) return reject( err );
            results = unflattenPairs( results );
            this._redisClient.mget(
              results.map( ( [ id ] ) => prefix( id ) ),
              ( err, tasks ) => {
                if ( err ) reject( err );
                const schedules = [];
                results.forEach( ([ id, start ], i ) => {
                  if ( tasks[ i ] ) {
                    schedules.push( [ +start, JSON.parse( tasks[ i ] ) ] );
                  }
                } );
                resolve( schedules );
              }
            )
          } );
    } );
  }

}

module.exports = Timetable;
