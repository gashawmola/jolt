
const WORK_INTERVAL = 1000;
const DELTA = 300;

const randInterval = function () {
  return Math.floor(
    ( WORK_INTERVAL - DELTA ) + Math.random() * Math.floor( 2 * DELTA + 1 )
  );
};

class Worker {

  constructor ( runQueue, tasks ) {
    this._runQueue = runQueue;
    this._interval = null;
    this._tasks = tasks;
    this.work = this.work.bind( this );
  }

  async work () {
    while ( true ) {
      const head = await this._runQueue.dequeue();
      if ( !head ) break;
      const [ task, params ] = head;
      const taskFunc = this._tasks[ task ];
      if ( !task ) {
        console.log( 'Task not found: ' + task );
        continue;
      }
      try {
        await taskFunc( params );
      } catch ( e ) {
        console.log( 'Error: ' + e );
      }
    }
  }

  start () {
    this._interval = setInterval( this.work, randInterval() );
  }

  stop () {
    if ( this._interval ) {
      clearInterval( this._interval );
      this._interval = null;
    }
  }

}

module.exports = Worker;
