module.exports = function () {
  console.log('Task: Clearing caches...');
  return new Promise( (resolve) => setTimeout(resolve, 2000) );
};
