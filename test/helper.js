const expect = require('chai').expect;
const redis = require('redis');

const createRedisClient = function () {
  return redis.createClient( 'redis://127.0.0.1:6379/1' );
};

global.expect = expect;
global.createRedisClient = createRedisClient;
