const RunQueue = require('../lib/run-queue');

describe( 'RunQueue', function () {

  let runQueue, redisClient;

  before( function () {
    redisClient = createRedisClient();
  } );

  beforeEach( function () {
    runQueue = new RunQueue( redisClient );
  } );

  afterEach( function ( done ) {
    redisClient.flushdb( done );
  } );

  after( function () {
    redisClient.quit();
  } );

  describe( 'FIFO', function () {

    it( 'adds tasks at the tail of the queue and removes from the head', async function () {
      const firstTask = 'send-welcome-email',
        firstTaskParams = { userId: 1 },
        secondTask = 'foo',
        secondTaskParams = { bar: 'baz' };
      await runQueue.enqueue( firstTask, firstTaskParams );
      await runQueue.enqueue( secondTask, secondTaskParams );
      const [ task1, params1 ] = await runQueue.dequeue();
      const [ task2, params2 ] = await runQueue.dequeue();
      const last = await runQueue.dequeue();
      expect( task1 ).to.equal( firstTask );
      expect( params1 ).to.deep.equal( firstTaskParams );
      expect( task2 ).to.equal( secondTask );
      expect( params2 ).to.deep.equal( secondTaskParams );
      expect( last ).to.be.a( 'null' );
    } );

  } );

} );
