const Scheduler = require('../lib/scheduler');
const sinon = require('sinon');

describe( 'Scheduler', function () {

  let scheduler;
  let timetable = {
    peak () {},
    get () {},
    remove () {},
    increment () {}
  };
  let runQueue = {
    enqueue () {}
  };

  beforeEach( function () {
    scheduler = new Scheduler( timetable, runQueue );
  } );

  describe( 'schedule()', function () {

    let timetableMock, runQueueMock;

    beforeEach( function () {
      const now = ( new Date() ).getTime();
      const task1 = { id: '1', task: 'task1', start: now - 1000, interval: 0, params: {} };
      const task2 = { id: '2', task: 'task2', start: now - 500, interval: 10000, params: {} };
      const task3 = { id: '3', task: 'task3', start: now + 60000, interval: 0, params: {} }; // not safe ...
      timetableMock = sinon.mock( timetable );
      runQueueMock = sinon.mock( runQueue );
      timetableMock.expects( 'peak' ).thrice()
        .onFirstCall().resolves( [ task1.id, task1.start ] )
        .onSecondCall().resolves( [ task2.id, task2.start ] )
        .onThirdCall().resolves( [ task3.id, task3.start ] );
      timetableMock.expects( 'get' ).twice()
        .onFirstCall().resolves( task1 )
        .onSecondCall().resolves( task2 )
        .onThirdCall().resolves( task3 );
      timetableMock.expects( 'remove' ).withArgs( task1.id ).once().resolves();
      timetableMock.expects( 'increment' ).withArgs( task2.id, task2.interval ).once().resolves();
      runQueueMock.expects( 'enqueue' ).once().withArgs( task1.task, task1.params ).resolves();
      runQueueMock.expects( 'enqueue' ).once().withArgs( task2.task, task2.params ).resolves();
    } );

    it( 'schedules all the tasks whose due time has passed', async function () {
      await scheduler.schedule();
      timetableMock.verify();
      runQueueMock.verify();
    });

  } );

  describe( 'start()/stop()', function () {

    let clock, scheduleStub;

    beforeEach( function () {
      clock = sinon.useFakeTimers();
      scheduleStub = sinon.stub( scheduler, 'schedule' );
    } );

    afterEach( function () {
      clock.restore();
      scheduleStub.restore();
    } );

    it( 'starts and stops the scheduler from scheduling in time intervals', function () {
      scheduler.start();
      clock.tick( 2000 );
      expect( scheduleStub.calledTwice ).to.be.ok;
      scheduler.stop();
      scheduleStub.resetHistory();
      clock.tick( 2000 );
      expect( scheduleStub.called ).to.not.be.ok;
    } );

  } );

} );
