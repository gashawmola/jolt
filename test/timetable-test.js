const Timetable = require('../lib/timetable');

describe( 'Timetable', function () {

  let timetable, redisClient;

  before( function () {
    redisClient = createRedisClient();
  } );

  beforeEach( function () {
    timetable = new Timetable( redisClient );
  } );

  afterEach( function ( done ) {
    redisClient.flushdb( done );
  } );

  after( function () {
    redisClient.quit();
  } );

  describe( 'add()', function () {

    it( 'adds the task to the timetable and returns an id', async function () {
      const task = 'update-sitemap',
        start = ( new Date() ).getTime(),
        interval = 28800000;
      const id = await timetable.add( task, { start, interval } );
      expect( id ).to.be.a( 'string' );
      const schedule = await timetable.get( id );
      expect( schedule ).to.deep.equal( { id, task, start, interval, params: {} } );
    } );

  } );

  describe( 'get()', function () {

    let schedule = {
      task: 'send-welcome-email',
      start: ( new Date() ).getTime(),
      interval: 0,
      params: {
        userId: 1
      }
    };

    beforeEach( async function () {
      schedule.id = await timetable.add( schedule.task, { ...schedule } );
    } );

    it( 'returns the task schedule for the given id', async function () {
      const storedSchedule = await timetable.get( schedule.id );
      expect( storedSchedule ).to.deep.equal( schedule );
    } );

  } );

  describe( 'remove()', function () {

    let scheduleId;

    beforeEach( async function () {
      scheduleId = await timetable.add( 'bar' );
    } );

    it( 'removes the task with given id from the timetable', async function () {
      await timetable.remove( scheduleId );
      const schedule = await timetable.get( scheduleId );
      expect( schedule ).to.be.a( 'null' );
    } );

  } );

  describe( 'peak()', function () {

    let nextId;
    const now = ( new Date() ).getTime();

    beforeEach( async function () {
      await timetable.add( 'foo', { start: now + 1000 } );
      nextId = await timetable.add( 'next', { start: now } );
      await timetable.add( 'bar', { start: now + 2000 } );
    } );

    it( 'retruns the id and next timestamp of the task with the earliest schedule time', async function () {
      const [ id, start ] = await timetable.peak();
      expect( id ).to.equal( nextId );
      expect( start ).to.equal( now );
    } );

  } );

  describe( 'increment()', function () {

    let scheduleId, start = ( new Date() ).getTime();

    beforeEach( async function () {
      scheduleId = await timetable.add( 'baz', { start } );
    } );

    it( 'changes the next timestamp of the task schedule for the given id', async function () {
      const delta = 60000;
      await timetable.increment( scheduleId, delta );
      const [ nextId, timestamp ] = await timetable.peak();
      expect( nextId ).to.equal( scheduleId );
      expect( timestamp ).to.equal( start + delta );
    } );

  } );

} );
