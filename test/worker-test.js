const Worker = require('../lib/worker');
const sinon = require('sinon');

describe( 'Worker', function () {

  let worker;
  let runQueue = {
    dequeue () {}
  };
  let tasks = {
    [ 'send-welcome-email' ] () {},
    [ 'clear-cache' ] () {}
  };

  beforeEach( function () {
    worker = new Worker( runQueue, tasks );
  } );

  describe( 'work()', function () {

    let runQueueMock, tasksMock;

    beforeEach( function () {
      queue = [ [ 'send-welcome-email', {} ], [ 'clear-cache', { hard: true } ] ];
      runQueueMock = sinon.mock( runQueue );
      tasksMock = sinon.mock( tasks );
      runQueueMock.expects( 'dequeue' ).thrice()
        .onFirstCall().resolves( queue[0] )
        .onSecondCall().resolves( queue[1] )
        .onThirdCall().resolves( null );
      tasksMock.expects( 'send-welcome-email' ).once().withArgs({});
      tasksMock.expects( 'clear-cache' ).once().withArgs( { hard: true } );
    } );

    it( 'works on all the enqueued tasks', async function () {
      await worker.work();
      runQueueMock.verify();
      tasksMock.verify();
    } );

  } );

  describe( 'start()/stop()', function () {

    let clock, workStub;

    beforeEach( function () {
      clock = sinon.useFakeTimers();
      workStub = sinon.stub( worker, 'work' );
    } );

    afterEach( function () {
      clock.restore();
      workStub.restore();
    } );

    it( 'starts and stops the worker from working in time intervals', function () {
      worker.start();
      clock.tick( 2000 );
      expect( workStub.called ).to.be.ok;
      worker.stop();
      workStub.resetHistory();
      clock.tick( 2000 );
      expect( workStub.called ).to.not.be.ok;
    } );

  } );

} );
